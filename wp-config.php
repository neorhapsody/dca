<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'dca_cms');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'dca');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'Q!w2E#r4T%');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ad6i^fK Ds)#m; raw{{N`uTLJ{fhQ/(tg7ey^^#BQ=uI+V,J0>co_i#1J?EHlWw');
define('SECURE_AUTH_KEY',  ')jw#n5^Y-})y?K5&CCG4kPW6|(2796n/OnG[Fy&_>^ZJ<z6|xjJ-Ms951Ri;;7S+');
define('LOGGED_IN_KEY',    'B+{5/{Ka-A-ne<b^x/TL{ee<b(-(6>poA52acV-Rxz?Ge#>@|xD+xefKw_=8E+T|');
define('NONCE_KEY',        'i9i[XmWGK=C}n]5:#YOT=*-#Si:HHc{30kp%Wyoj{]eY@:,/ghK$oqrCmA4+N ;E');
define('AUTH_SALT',        'yp<RJ*/z$s7s|*=X$V9j-.n/!O{Jeu+o-|tU`3,e+19=(|?$x1u`B=DNWw8z59ip');
define('SECURE_AUTH_SALT', 'j:]Z|!ET cQ3)*8P=OS(#BYFutLwIMp &PXznqKffBQMUH6Lre&#)ZDLlY&Zvz~T');
define('LOGGED_IN_SALT',   '02jIQFm/S]F:$tAVos/__Rf{%wA@ZC,`d1b7HUZQWhH1IZy b|.s|w9qf=g$bO!@');
define('NONCE_SALT',       'fbY8]4n8b$ joiz 17zlXd(AQMWD9T8E|o6)H@;8@)a%,t!O7s?v2x;^g2bB.,,h');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

define('WP_DEBUG_LOG',true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
