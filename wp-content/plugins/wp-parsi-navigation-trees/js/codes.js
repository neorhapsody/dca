jQuery(document).ready(function(){

	jQuery('#wb_tree ul li').has('ul.sub-menu').prepend('<span>+</span>');
	jQuery('#wb_tree ul ul').filter(function(){
    jQuery(this).has('.current-menu-item').parent().css('background','none').find('span').text('-');
    return $(this).has('.current-menu-item').length === 0;
  }).hide();
	
  jQuery('#wb_tree span').click(function(){
    var isHidden = jQuery(this).text() === '+';
    jQuery(this).text(isHidden ? '-' : '+');
    var parent = jQuery(this).parent();
    !isHidden ? parent.css('background','url("../images/L.png"') : parent.css('background','none');
    jQuery(parent).find('>ul').slideToggle();

    isHidden = !isHidden;
  });


});
