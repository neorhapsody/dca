<aside id="sidebar" class="small-12 large-4 columns">
	<?php do_action('foundationPress_before_sidebar'); ?>
	<div id="sector-division-menu" class="small-12 large-12 columns borderize white-bg row-gap-10">
		<h5>Sectors &amp; Division</h5>
		<hr>
		<?php
			$menu_items = get_menu_items('sector-division-menu');
		?>
		<ul class="sector-division-slider" data-orbit data-options="timer:false;bullets:false;slide_number:false;pause_on_hover:false;">
			<?php $count = 1; ?>
			<?php foreach (array_chunk($menu_items, 5) as $menus) : ?>
				<li data-orbit-slide="headline-<?=$count?>">
				<?php foreach ($menus as $menu) : ?>
				    <a href="<?=$menu->url?>">
						<i class="fa fa-plane"></i>&nbsp;<span><?=$menu->title?></span>
				    </a><br>
				<?php endforeach; ?>
				</li>
			<?php $count++;endforeach; ?>
		</ul>
	</div>

	<div class="small-12 large-12 columns borderize white-bg row-gap-10">
		<h5 id="online-services-header" style="text-align:center">Online Services</h5>
	</div>
	
	<?php dynamic_sidebar("Online Services"); ?>

	<div id="external-links" class="small-12 large-12 columns borderize white-bg row-gap-10 ">
		<ul class="external-links-slider" data-orbit data-options="animation:slide;
                  pause_on_hover:false;
                  animation_speed:500;
                  navigation_arrows:true;
                  bullets:false;
                  timer_show_progress_bar:false;
                  timer:false;
                  slide_number:false;" >
          <li>
          	<div class="small-4 large-4 columns"><a href="http://www.mscmalaysia.my/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/external/msc.jpg" alt="MSC logo"></a></div>
          	<div class="small-4 large-4 columns"><a href="https://www.myeg.com.my/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/external/myeg.png" alt="MYEG logo"></a></div>
          	<div class="small-4 large-4 columns"><a href="https://www.malaysia.gov.my/en"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/external/mygovernment.png" alt="MyGovernment logo"></a></div>
          </li>
          <li>
          	<div class="small-4 large-4 columns"><a href="http://www.mscmalaysia.my/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/external/msc.jpg" alt="MSC logo"></a></div>
          	<div class="small-4 large-4 columns"><a href="https://www.myeg.com.my/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/external/myeg.png" alt="MYEG logo"></a></div>
          	<div class="small-4 large-4 columns"><a href="https://www.malaysia.gov.my/en"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/external/mygovernment.png" alt="MyGovernment logo"></a></div>
          </li>
		</ul>
	</div>

	<div id="contact-us" class="small-12 large-12 columns borderize white-bg row-gap-10">
		<h5>Contact Us</h5>
		<hr>
		<ul class="vcard">
			<li class="building-name">Department of Civil Aviation</li>
			<li class="street-address">No. 27, Persiaran Perdana</li>
			<li class="street-address">Block Podium, Precinct 4</li>
			<li class="locality"><span class="zip">62618</span> <span class="state">PUTRAJAYA</span></li>
			<li class="country">MALAYSIA</li>
			<li>&nbsp;</li>
			<li>&nbsp;</li>
			<li>Tel: <span class="phone-no">+603-88714000</span></li>
			<li class="email">Email: <a href="mailto:webmaster@dca.gov.my">webmaster@dca.gov.my</a></li>
		</ul>
		<br>
	</div>

	<div class="small-12 large-12 columns borderize white-bg row-gap-10 common-header">
		<h5>Follow Us On</h5>
	</div>

	<div class="small-12 large-12 columns white-bg row-gap-10">
		<?php dynamic_sidebar("sidebar-widgets"); ?>
	</div>

	<?php do_action('foundationPress_after_sidebar'); ?>
</aside>