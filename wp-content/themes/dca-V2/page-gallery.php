<?php get_header(); ?>
	<br>
	<div class="small-12 large-12 columns" role="main">
	
	<?php do_action('foundationPress_before_content'); ?>
	
	<ul class="small-block-grid-3 clearing-thumbs" data-clearing>
	<?php foreach(get_images_with_acf('image_gallery') as $image): ?>
	<li>
		<a href="<?=wp_get_attachment_url($image->ID)?>">
			<img data-caption="<?=$image->post_title?>" src="<?=wp_get_attachment_thumb_url($image->ID)?>" alt="<?=$image->post_title?>">
		</a>
	</li>
	<?php endforeach; ?>
	</ul>

	<?php do_action('foundationPress_after_content'); ?>

	</div>
		
<?php get_footer(); ?>