<?php get_header(); ?>
	<br>
	<?php 
		$images = get_images_with_acf('slider_images');
		if(count($images)):
	?>
	<ul id="home-slider" data-orbit data-options="animation:slide;
                  pause_on_hover:false;
                  animation_speed:500;
                  navigation_arrows:true;
                  bullets:true;" >
		<?php foreach ($images as $image) : ?>
		  <li>
		    <img src="<?= wp_get_attachment_url($image->ID) ?>" alt="<?= $image->post_title ?>" />
		    <div class="orbit-caption">
		      <?= $image->post_excerpt ?>
		    </div>
		  </li>
		<?php endforeach; ?>
	</ul>
	<br>
	<?php endif; ?>
	<div class="small-12 large-8 columns" role="main">

		<div class="small-12 large-12 columns white-bg drop-padding row-gap-10">
			<div id="first-content-tabs" class="content-tabs">
				<dl class="tabs" data-tab>
					<dd class="active small-4 large-4 columns"><a href="#highlights">Highlights</a></dd>
					<dd class="small-4 large-4 columns"><a href="#news">News</a></dd>
					<dd class="small-4 large-4 columns"><a href="#tender-quotation">Tenders &amp; Quotations</a></dd>
				</dl>
				<div class="tabs-content">
					<div class="content active" id="highlights">
						<ul data-equalizer-watch>
							<?php foreach (get_highlighted_posts() as $post): ?>
								<li>
									<h5><a href="<?=get_permalink($post->ID)?>"><?= $post->post_title ?></a></h5>
									<p><?= wp_trim_words( $post->post_content, 40, '<a href="'. get_permalink() .'"> ...Read More</a>' ) ?></p>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
					<div class="content" id="news">
						<ul>
							<?php foreach (get_news_posts() as $post): ?>
								<li>
									<h5><a href="<?=get_permalink($post->ID)?>"><?= $post->post_title ?></a></h5>
									<p><?= wp_trim_words( $post->post_content, 40, '<a href="'. get_permalink() .'"> ...Read More</a>' ) ?></p>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
					<div class="content" id="tender-quotation">
						<ul>
						<?php foreach (get_posts_by_category('tender') as $post): ?>
							<li>
									<h5><a href="<?=get_permalink($post->ID)?>"><?= $post->post_title ?></a></h5>
								<p><?= wp_trim_words( $post->post_content, 40, '<a href="'. get_permalink() .'"> ...Read More</a>' ) ?></p>
							</li>
						<?php endforeach; ?>	
						</ul>	
					</div>
				</div>	
			</div>	
		</div>
	
		<div class="small-12 large-12 columns white-bg drop-padding row-gap-10">
			<div id="second-content-tabs" class="content-tabs">
				<dl class="tabs" data-tab>
					<dd class="active small-4 large-4 columns"><a href="#publications">Publications</a></dd>
					<dd class="small-4 large-4 columns"><a href="#forms">Forms</a></dd>
					<dd class="small-4 large-4 columns"><a href="#examinations">Examinations</a></dd>
				</dl>
				<div class="tabs-content">
					<div class="content active" id="publications">
						<ul>
							<?php foreach (get_menu_items('publication-menu') as $item): ?>
								<li>
									<h5><a href="<?=$item->url?>"><?= $item->title ?></a></h5>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
					<div class="content" id="forms">
						<ul>
							<?php foreach (get_menu_items('forms-menu') as $item): ?>
								<li>
									<h5><a href="<?=$item->url?>"><?= $item->title ?></a></h5>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
					<div class="content" id="examinations">
						<ul>
						<?php foreach (get_menu_items('examinations-menu') as $item): ?>
								<li>
									<h5><a href="<?=$item->url?>"><?= $item->title ?></a></h5>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>	
			</div>
		</div>

		<div class="small-12 large-12 columns white-bg drop-padding row-gap-10">
			<div class="small-12 large-6 columns drop-padding-left gallery">

				<div class="small-12 large-12 columns borderize white-bgdrop-padding-left">
					<h5 class="text-center" style="font-weight:bold"><?php echo __('Gallery') ?></h5>
				</div>

				<ul style="list-style:none;margin-left:0;margin-top:45px;padding:10px">
					<li class="text-center">
					<?php 
						$images = get_images_with_acf('image_gallery');
						$size = count($images);
						$url = 'http://placehold.it/350x180';
						$title = 'placeholder';
						if($size)
						{
							$image = $images[rand(0,$size - 1)];
							$url = wp_get_attachment_thumb_url($image->ID);
							$title = $image->post_title;
						}
					?>
					<img style="height:160px;width:100%" src="<?= $url ?>" alt="<?= $title ?>" />
					<a style="display:block;text-align:right" href="/gallery">More Picture &gt;</a>
					</li>
				</ul>
				<ul style="list-style:none;margin-left:0;padding:10px">
					<li class="text-center">
						<iframe width="100%" src="//www.youtube.com/embed/H57PafYnbpw" frameborder="0" allowfullscreen></iframe>
						<a style="display:block;text-align:right" href="//http://www.youtube.com/user/DCAWEB01">More Videos &gt;</a>
					</li>
				</ul>
			</div>
			<div id="polls-widget" class="small-12 large-6 columns">
				<?php //if ( dynamic_sidebar('Polls Widget') ) : else : endif; ?>
				<iframe src="https://www.facebook.com/connect/connect.php?id=1442799722633187&connections=10&stream=1" scrolling="yes" frameborder="0" style="border-bottom:1px solid #aaa; overflow:hidden; width:100%; height:445px;" allowTransparency="true"></iframe>

			</div>
		</div>
		
		
	
	</div>
	<?php get_sidebar(); ?>

	<?php if ( dynamic_sidebar('Footer External Links') ) : else : endif; ?>
<?php get_footer(); ?>