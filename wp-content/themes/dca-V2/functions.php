<?php
/*
Author: Ole Fredrik Lie
URL: http://olefredrik.com
*/


// Various clean up functions
require_once('library/cleanup.php'); 

// Required for Foundation to work properly
require_once('library/foundation.php');

// Register all navigation menus
require_once('library/navigation.php');

// Add menu walker
require_once('library/menu-walker.php');

// Create widget areas in sidebar and footer
require_once('library/widget-areas.php');

// Return entry meta information for posts
require_once('library/entry-meta.php');

// Enqueue scripts
require_once('library/enqueue-scripts.php');

// Add theme support
require_once('library/theme-support.php');

// Add theme specific widgets
require_once('library/widgets/include.php');

function is_last_item(&$array, $keyOrIndex)
{
	end($array);
	return $keyOrIndex === key($array);
}

function get_images_with_acf($field_name){
	$query_images_args = array(
	    'post_type' => 'attachment', 'post_mime_type' =>'image', 'post_status' => 'inherit', 'posts_per_page' => -1,
	);

	$query_images = new WP_Query( $query_images_args );
	$images = array();
	foreach ( $query_images->posts as $image) {
		if(get_field($field_name, $image->ID))
		{
			$images[]= $image;
		}
	}
	return $images;
}


function get_posts_by_category($cat)
{
	$args = array(
			'posts_per_page'   => 3,
			'offset'           => 0,
			'orderby'          => 'post_date',
			'order'            => 'DESC',
			'post_type'        => 'post',
			'post_status'      => 'publish',
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'category',
					'field' => 'slug',
					'terms' => array( $cat )
				),
				array(
					'taxonomy' => 'category',
					'field' => 'slug',
					'terms' => array( 'archived' ),
					'operator' => 'NOT IN'
				)
			)
		);
	$query = new WP_Query($args);
	return $query->have_posts() ? $query->posts : array();

}

function get_highlighted_posts(){
	return get_posts_by_category('highlight');
}

function get_news_posts(){
	return get_posts_by_category('news');
}

function get_menu_items($menu_name)
{
	$menu = wp_get_nav_menu_object( $menu_name );
	$menu_items = wp_get_nav_menu_items( $menu->term_id );

	return $menu_items;
}


function register_my_menu() {
  register_nav_menu('sector-division-menu',__( 'Sector and Division Menu' ));
  register_nav_menu('footer-menu',__('Footer Menu'));
}
add_action( 'init', 'register_my_menu' );

//register polls widget
register_sidebar(array(
  'name' => __( 'Polls Widget' ),
  'id' => 'polls-widget',
  'description' => __( 'A section to show polls widget' ),
  'before_title' => '',
  'after_title' => ''
));

register_sidebar(array(
  'name' => __( 'Translate Widget' ),
  'id' => 'translate-widget',
  'description' => __( 'A section to show translator widget' ),
  'before_title' => '',
  'after_title' => ''
));

register_sidebar(array(
  'name' => __( 'Left Sidebar Widgets' ),
  'id' => 'left-sidebar-widgets',
  'description' => __( 'A left sidebar container for widgets' ),
  'before_title' => '',
  'after_title' => ''
));

register_sidebar(array(
  'name' => __( 'Footer External Links' ),
  'id' => 'footer-external-links-widget',
  'description' => __( 'A section to show external service widget' ),
  'before_title' => '',
  'after_title' => ''
));

register_sidebar(array(
  'name' => __( 'Online Services' ),
  'id' => 'online-services-widget',
  'description' => __( 'A section to show external service widget' ),
  'before_title' => '',
  'after_title' => ''
));



?>