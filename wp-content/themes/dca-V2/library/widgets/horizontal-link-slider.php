<?php
class Horizontal_Link_Slider extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	const ITEMS_PER_PAGE = 'items per page';
	public function __construct() {
		parent::__construct(
			'horizontal-link-slider', // Base ID
			__('Horizontal Link Slider', 'horizontal link slider widget'), // Name
			array( 'description' => __( 'A horizontal image link slider', 'horizontal link slider widget' ), ) // Args
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		// outputs the content of the widget
		?>
		<div id="external-links" class="small-12 large-12 columns borderize white-bg row-gap-10 ">
		<ul class="external-links-slider" data-orbit data-options="animation:slide;
                  pause_on_hover:false;
                  animation_speed:500;
                  navigation_arrows:true;
                  bullets:false;
                  timer_show_progress_bar:false;
                  timer:false;
                  slide_number:false;" >
        <?php 
        	$images = $this->get_value($instance, 'link_image', array());
			$urls = $this->get_value($instance, 'url', array());
			$items_per_page = $this->get_value($instance, 'number_of_items_per_page', 0);
			$class_size = floor(12/$items_per_page);
			$class = "small-$class_size large-$class_size columns";
			$links = array_combine($urls, $images);
        	foreach(array_chunk($links, $items_per_page, true) as $chunks)
        	{
        		echo '<li>';
        		foreach ($chunks as $url => $image) : ?>
        			<div class="<?=$class?>">
        				<a href="<?=$url?>">
        					<img src="<?=$image?>">
        				</a>
        			</div>
				<?php
        		endforeach;
        		echo '</li>';
        	}
        ?>
			</ul>
			</div>
		<?php

	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {

		$number_of_items = $this->get_value($instance, 'number_of_items', 0);
		$number_of_items_per_page = $this->get_value($instance, 'number_of_items_per_page', 0);

		?>

		<label for="<?php echo $this->get_field_id( 'number_of_items' ); ?>"><?php _e( 'Number of Items:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'number_of_items' ); ?>" name="<?php echo $this->get_field_name( 'number_of_items' ); ?>" type="text" value="<?php echo esc_attr( $number_of_items ); ?>">

		<label for="<?php echo $this->get_field_id( 'number_of_items_per_page' ); ?>"><?php _e( 'Number of Items per Page:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'number_of_items_per_page' ); ?>" name="<?php echo $this->get_field_name( 'number_of_items_per_page' ); ?>" type="text" value="<?php echo esc_attr( $number_of_items_per_page ); ?>">

		<?php
		if($number_of_items > 0)
		{
			$images = $this->get_value($instance, 'link_image', array());
			$urls = $this->get_value($instance, 'url', array());

			$count = 0;

			while($number_of_items--) :
			?>
				<p>
				<b>Item number <?=$count+1?></b>
				<br>
				<label for="<?php echo $this->get_field_id( 'link_image' ); ?>"><?php _e( 'Image URL:' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'link_image' ); ?>" name="<?php echo $this->get_field_name( 'link_image' ); ?>[]" type="text" value="<?php echo esc_attr( $images[$count] ); ?>">

				<label for="<?php echo $this->get_field_id( 'url' ); ?>"><?php _e( 'URL:' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'url' ); ?>" name="<?php echo $this->get_field_name( 'url' ); ?>[]" type="text" value="<?php echo esc_attr( $urls[$count] ); ?>">	
				</p>

			<?php
				$count++;
			endwhile;

		}
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		return $new_instance;
	}

	private function get_value($instance, $name, $default = '')
	{
		if ( isset( $instance[ $name ] ) ) {
			return $instance[ $name ];
		} else {
			return $default;
		}
	}
}