<?php
class Online_Services extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	const ITEMS_PER_PAGE = 'items per page';
	public function __construct() {
		parent::__construct(
			'online-services', // Base ID
			__('Online Services', 'online services widget'), // Name
			array( 'description' => __( 'An online services link', 'online services widget' ), ) // Args
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		// outputs the content of the widget
		$mids_url = $this->get_value($instance, 'mids_url', '');
		$eame_url = $this->get_value($instance, 'eame_url', '');
		$efc_url = $this->get_value($instance, 'efc_url', '');
		?>
		<div id="online-services" class="small-12 large-12 columns text-center row-gap-20">
			<div class="small-4 large-4 columns">
				<a href="<?=$mids_url?>"><i class="fa fa-plane text-blue"></i> <br> <span class="border-blue">MIDS</span></a>
			</div>
			<div class="small-4 large-4 columns">
				<a href="<?=$eame_url?>"><i class="fa fa-plane text-red"></i> <br> <span class="border-red">e-AME</span></a>
			</div>
			<div class="small-4 large-4 columns">
				<a href="<?=$efc_url?>"><i class="fa fa-plane text-yellow"></i> <br> <span class="border-yellow">e-FC</span></a>
			</div>
		</div>
		<?php

	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {

		$mids_url = $this->get_value($instance, 'mids_url', '');
		$eame_url = $this->get_value($instance, 'eame_url', '');
		$efc_url = $this->get_value($instance, 'efc_url', '');

		?>

		<label for="<?php echo $this->get_field_id( 'mids_url' ); ?>"><?php _e( 'MIDS URL:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'mids_url' ); ?>" name="<?php echo $this->get_field_name( 'mids_url' ); ?>" type="text" value="<?php echo esc_attr( $mids_url ); ?>">

		<label for="<?php echo $this->get_field_id( 'eame_url' ); ?>"><?php _e( 'e-AME URL:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'eame_url' ); ?>" name="<?php echo $this->get_field_name( 'eame_url' ); ?>" type="text" value="<?php echo esc_attr( $eame_url ); ?>">

		<label for="<?php echo $this->get_field_id( 'efc_url' ); ?>"><?php _e( 'e-FC URL:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'efc_url' ); ?>" name="<?php echo $this->get_field_name( 'efc_url' ); ?>" type="text" value="<?php echo esc_attr( $efc_url ); ?>">

		<?php
		
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		return $new_instance;
	}

	private function get_value($instance, $name, $default = '')
	{
		if ( isset( $instance[ $name ] ) ) {
			return $instance[ $name ];
		} else {
			return $default;
		}
	}
}