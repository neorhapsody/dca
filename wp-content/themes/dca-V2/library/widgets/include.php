<?php


// register Foo_Widget widget
function register_theme_widgets() {
	require_once('horizontal-link-slider.php');
    register_widget( 'Horizontal_Link_Slider' );


	require_once('online-services.php');
    register_widget( 'Online_Services' );
}
add_action( 'widgets_init', 'register_theme_widgets' );