<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php if ( is_category() ) {
      echo 'Category Archive for &quot;'; single_cat_title(); echo '&quot; | '; bloginfo( 'name' );
    } elseif ( is_tag() ) {
      echo 'Tag Archive for &quot;'; single_tag_title(); echo '&quot; | '; bloginfo( 'name' );
    } elseif ( is_archive() ) {
      wp_title(''); echo ' Archive | '; bloginfo( 'name' );
    } elseif ( is_search() ) {
      echo 'Search for &quot;'.esc_html($s).'&quot; | '; bloginfo( 'name' );
    } elseif ( is_home() || is_front_page() ) {
      bloginfo( 'name' ); echo ' | '; bloginfo( 'description' );
    }  elseif ( is_404() ) {
      echo 'Error 404 Not Found | '; bloginfo( 'name' );
    } elseif ( is_single() ) {
      wp_title('');
    } else {
      echo wp_title( ' | ', 'false', 'right' ); bloginfo( 'name' );
    } ?></title>
    
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app.css" />
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/apple-touch-icon-144x144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/apple-touch-icon-precomposed.png">
    
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
  <?php do_action('foundationPress_after_body'); ?>
  
  <div class="off-canvas-wrap">
  <div class="inner-wrap">
  
  <?php do_action('foundationPress_layout_start'); ?>
  
  <nav class="tab-bar show-for-small-only">
    <section class="left-small">
      <a class="left-off-canvas-toggle menu-icon" ><span></span></a>
    </section>
    <section class="middle tab-bar-section">
      
      <h1 class="title"><?php bloginfo( 'name' ); ?></h1>

    </section>
  </nav>

<header class="row" role="banner">
  <div id="accessibility-tools" class="small-12 large-12 columns">
      <div class="tools font-color-red"></div>
      <div class="tools font-color-green"></div>
      <div class="tools font-color-blue"></div>
      <div class="tools font-color-reset"></div>
      <div class="tools font-size-increase"></div>
      <div class="tools font-size-reset"></div>
      <div class="tools font-size-decrease"></div>
      <?php if ( dynamic_sidebar('Translate Widget') ) : else : endif; ?>
      <div class="right"><?= date('D, j M Y h:i A')?></div>
  </div>
  <div class="small-12 columns top-header-container">   
    <div class="small-3 columns text-center" style="padding: 0px;"> 
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo_tiger.png" alt="Jata Negara">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo_dca.png" alt="DCA logo" width="50%">
    </div>
    <div class="small-9 columns row-gap-10">
      <h4 class="subheader text-orange"><?php bloginfo('description'); ?></h4>
      <h1 style="position: absolute;top: 15px;"><a  class="text-blue" href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h1>
    </div>
  </div>
</header>

<aside class="left-off-canvas-menu">
  <?php foundationPress_mobile_off_canvas(); ?>
</aside>

<section class="container" role="document">
  <?php do_action('foundationPress_after_header'); ?>
  <div class="row">
    <div class="top-bar-container contain-to-grid show-for-medium-up">
    <nav class="top-bar" data-topbar="">
        <section class="top-bar-section">
            <?php foundationPress_top_bar_l(); ?>
            <ul class="top-bar-menu right">
              <li class="has-form" style="padding-right:0px">
                <?php get_search_form(); ?>
            </ul>
            <?php foundationPress_top_bar_r(); ?>
        </section>

    </nav>
</div>