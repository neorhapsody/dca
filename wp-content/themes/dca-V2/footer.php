	</div>
	<br><br>
</section>

<footer class="row">
	<?php do_action('foundationPress_before_footer'); ?>
	<div class="small-12 large-12 columns">
		<nav id="sitelinks" class="text-center">
			<ul>
				<?php $sitelinks = get_menu_items('footer-links'); ?>
				<?php foreach ($sitelinks as $idx => $link): ?>
					<li><a href="<?=$link->url?>"><?=$link->title?></a>
					<?php if (!is_last_item($sitelinks, $idx)): ?> 
						 &nbsp;|&nbsp;
					<?php endif ?>
					</li>
				<?php endforeach ?>
			</ul>
		</nav>
	</div>
	<br><br>
	<div class="small-12 large-12 columns text-center">
		Last Updated: <?php the_modified_date(); ?>
		<br>
		<p>Best viewed in Mozilla Firefox and Google Chrome</p>
		<p> <?=date('Y')?> &copy; Copyright Reserved Department of Civil Aviation Malaysia</p>
		<!-- BEGIN: Powered by Supercounters.com -->
		<div style="display:inline-block">Visitors: </div>
		<center style="display:inline-block">
			<script type="text/javascript" src="http://widget.supercounters.com/hit.js"></script>
			<script type="text/javascript">sc_hit(823273,4,5);</script><br>
			<noscript><a href="http://www.supercounters.com">Tumblr Hit Counter</a></noscript>
		</center>
		<!-- END: Powered by Supercounters.com -->
	</div>
	<?php dynamic_sidebar("footer-widgets"); ?>
	<?php do_action('foundationPress_after_footer'); ?>
</footer>
<a class="exit-off-canvas"></a>
	
  <?php do_action('foundationPress_layout_end'); ?>
  </div>
</div>
<?php wp_footer(); ?>
<?php do_action('foundationPress_before_closing_body'); ?>
<script>
	function allTextElements(){
		return $('p, a, li, h1, h2, h3, h4, h5, h6');
	}
	var newFontSize = 100;
	$(document).ready(function(){
	  //W3C accessibility
	  $(".tools.font-size-reset").click(function(){
	    newFontSize = 100;
	    allTextElements().css('font-size','');
	  });

	  $(".tools.font-size-increase").click(function(){
	    newFontSize = newFontSize*1.1;
	    allTextElements().css('font-size',newFontSize + '%');
	    return false;
	  });

	  $(".tools.font-size-decrease").click(function(){
	  	if(newFontSize == 100){ return; } //dont go lower than 100%
	    newFontSize = newFontSize/1.1;
	    allTextElements().css('font-size',newFontSize + '%');
	    return false;
	  });

	  $.each(['red','blue','green'], function(index, color){
	  	$(".tools.font-color-"+color).click(function(){
		  allTextElements().css('color',color);
		});
	  });

	  $(".tools.font-color-reset").click(function(){
	   allTextElements().css('color','');
	  });

	});
</script>
</body>
</html>