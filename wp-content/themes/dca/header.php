<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php wp_title(); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/components/foundation/css/normalize.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/components/foundation/css/foundation.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
	<?php wp_head(); ?>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/components/modernizr/modernizr.js"></script>
</head>
<body>
<div class="off-canvas-wrap">
  <div class="inner-wrap">
  	<!-- all content goes here -->
  	<div class="header-top">
  		
<div id="header" class="row">
	<div id="logo" class="small-12 medium-3 large-3 columns">
		<div>

		<img src="<?php echo get_template_directory_uri(); ?>/images/logo_tiger.png" alt="Jata Negara">
	<img width="100px" src="<?php echo get_template_directory_uri(); ?>/images/logo_dca.png" alt="Logo DCA">
			
		</div>
	</div>

	<div class="small-12 medium-5 large-5 columns">
		<div id="w3c" class="right">

			<div class="tools font-color-red"></div>
			<div class="tools font-color-green"></div>
			<div class="tools font-color-blue"></div>
			<div class="tools font-color-reset"></div>
			<div class="tools font-size-increase"></div>
			<div class="tools font-size-reset"></div>
			<div class="tools font-size-decrease"></div>

			<a href="#" data-dropdown="lang" class="mini button dropdown" style="margin-right:5px"><?php echo qtrans_getLanguageName(); ?></a>
			<ul id="lang" class="f-dropdown" data-dropdown-content>
			  <li><a href="/ms">Bahasa Malaysia</a></li>
			  <li><a href="/en">English</a></li>
			</ul>
		</div>
		<div class="clearfix"></div>
			<h5 id="site-title" class="left">
				<small>Official Portal</small> <br> 
				<strong>DEPARTMENT OF CIVIL AVIATION MALAYSIA <strong>
			</h5>
		</div>
		<div class="small-12 medium-4 large-4 columns">
			<div id="top-links">
				<?php if ( dynamic_sidebar('Header Widget') ) : else : endif; ?>
			</div>
			<?php echo get_search_form(); ?>
		</div>
	</div>

  	</div>
  	<div class="row">
	<nav class="no-js top-bar data-topbar">
		<ul class="title-area">
		    <li class="name"></li>
		    <li class="toggle-topbar menu-icon">
		    	<a class="right-off-canvas-toggle" >Menu</a>
		    </li>
		</ul>
		<section class="top-bar-section">
		<?php 
			$defaults = array(
				'theme_location'  => 'header-menu',
				'container'       => false,
				'echo'            => true,
				'fallback_cb'     => 'wp_page_menu',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			);
			wp_nav_menu( $defaults );
		?> 	
		</section>
	</nav>
	<aside class="right-off-canvas-menu"><?php 
			$defaults = array(
				'theme_location'  => 'header-menu',
				'menu_class'	  => 'off-canvas-list',
				'container'       => false,
				'echo'            => true,
				'fallback_cb'     => 'wp_page_menu',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			);
			wp_nav_menu( $defaults );
		?> 	
	</aside>
	<a class="exit-off-canvas"></a>
	</div>


