<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
	<div class="row collapse">
		<div class="large-8 columns"> <input type="text" value="" name="s" id="s" /></div>
		<div class="large-4 columns"> <input class="button postfix" type="submit" id="searchsubmit" value="Search" /></div>
	</div>
</form>