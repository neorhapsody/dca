function password_strength(password, div)
{
	var desc = new Array();
	desc[0] = "Very Weak";
	desc[1] = "Weak";
	desc[2] = "Better";
	desc[3] = "Medium";
	desc[4] = "Strong";
	desc[5] = "Strongest";

	var points = 0;

	//---- if password is bigger than 4 , give 1 point.
	if (password.length > 4) points++;

	//---- if password has both lowercase and uppercase characters , give 1 point.	
	if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) points++;

	//---- if password has at least one number , give 1 point.
	if (password.match(/\d+/)) points++;

	//---- if password has at least one special caracther , give 1 point.
	if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) )	points++;

	//---- if password is bigger than 12 ,  give 1 point.
	if (password.length > 12) points++;

	//---- Showing  description for password strength.
	document.getElementById(div).innerHTML = desc[points];
	
	//---- Changeing CSS class.
	document.getElementById(div).className = "strength" + points;
}