<?php
/**
 * The Template for displaying all search result
 * @package WordPress
 * @subpackage dca Theme
 */

get_header(); ?>
<div id="container" class="row">
	<div class="large-3 columns">
		<?php echo get_sidebar(); ?>
	</div>
	<div style="background-color:#fff;" class="small-12 large-9 columns">
		<h3 class="page-title"><?php printf( __( 'Search Results for: %s', 'dca theme' ), '<span>' . get_search_query()  . '</span>' ); ?></h3>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', get_post_format() ); ?>
		<?php endwhile; else: ?>
		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
		<?php endif; ?>
	</div>
</div>

<?php
get_footer();