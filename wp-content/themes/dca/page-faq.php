<?php
/**
 * The Template for displaying faq page
 *
 * @package WordPress
 * @subpackage 
 * @since 
 */

get_header(); ?>
<div id="container" class="row">
	<div class="large-3 columns">
		<?php echo get_sidebar('left'); ?>
	</div>
	<div style="background-color:#fff" class="small-12 large-9 columns">
		<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();

			?>
			<h3><?php the_title();?></h3>
			<hr>
			<ol>
			<?php
				$children = get_children(array(
					'post_parent' => get_the_ID(),
					'post_type' => 'page',
					'post_status' => 'publish'
					));

				foreach ($children as $childpage) {
				?>
					<li><a href="<?=get_permalink($childpage->ID)?>"><?=apply_filters( 'the_title', $childpage->post_title );?></a></li>	
				<?php
				}
			endwhile;
		?>
			</ol>
		
	</div>
</div>

<?php
get_footer();