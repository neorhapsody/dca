<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div id="container" class="row">
	<div class="large-3 columns">
		<?php echo get_sidebar( 'left' ); ?>
	</div>
	<div class="small-12 large-9 columns">
		<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();

			?>
			<h3><?php the_title();?></h3>
			<hr>
			<?php
				the_content();
			endwhile;
		?>
		
	</div>
</div>
<?php
get_footer();