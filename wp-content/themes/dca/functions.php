<?php

function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
  register_nav_menu('sector-division-menu',__( 'Sector and Division Menu' ));
  register_nav_menu('site-menu',__('Site Menu'));
}
add_action( 'init', 'register_my_menu' );

register_sidebar(array(
  'name' => __( 'Header Widget' ),
  'id' => 'header-widget',
  'description' => __( 'widget in this area will be shown in top right of the header' ),
  'before_title' => '',
  'after_title' => ''
));

register_sidebar(array(
  'name' => __( 'Home Page Center Widget' ),
  'id' => 'home-widget-center',
  'description' => __( 'widget in this area will be shown in the middle of the homepage' ),
  'before_title' => '',
  'after_title' => ''
));

register_sidebar(array(
  'name' => __( 'Home Page Widget 1' ),
  'id' => 'home-widget-1',
  'description' => __( 'widget in this area will be shown on the middle left' ),
  'before_title' => '',
  'after_title' => ''
));

register_sidebar(array(
  'name' => __( 'Home Page Widget 2' ),
  'id' => 'home-widget-2',
  'description' => __( 'widget in this area will be shown on the middle middle' ),
  'before_title' => '',
  'after_title' => ''
));

register_sidebar(array(
  'name' => __( 'Home Page Widget 3' ),
  'id' => 'home-widget-3',
  'description' => __( 'widget in this area will be shown on the middle right' ),
));

register_sidebar(array(
  'name' => __( 'Right Sidebar Widget' ),
  'id' => 'right-sidebar-widget',
  'description' => __( 'widget in this area will be shown in right sidebar' ),
));

register_sidebar(array(
  'name' => __( 'Left Sidebar Widget' ),
  'id' => 'left-sidebar-widget',
  'description' => __( 'widget in this area will be shown in left sidebar' ),
));


// register theme specific widgets

function register_theme_widgets(){

  require_once(get_template_directory().'/widgets/Post_Highlight.php');
    register_widget( 'Post_Highlights' );

  require_once(get_template_directory().'/widgets/Post_News.php');
    register_widget( 'Post_News' );

  require_once(get_template_directory().'/widgets/Event_Calendar.php');
    register_widget( 'Event_Calendar' );

  require_once(get_template_directory().'/widgets/Orbit_Slider.php');
    register_widget( 'Orbit_Slider' );

  require_once(get_template_directory().'/widgets/SectorDivision_Slider.php');
    register_widget( 'SectorDivision_Slider' );

  require_once(get_template_directory().'/widgets/Top_Links.php');
    register_widget( 'Top_Links' );

}
add_action( 'widgets_init', 'register_theme_widgets' );


function organization_chart_scripts(){
    wp_enqueue_style('jOrgChartCss',get_template_directory_uri() . '/components/jOrgChart/example/css/jquery.jOrgChart.css');
    wp_enqueue_script('jOrgChartScript',get_template_directory_uri() . '/components/jOrgChart/example/jquery.jOrgChart.js',array('jquery'),false,true);

    function jqueryOrgChartInit(){ 
      echo '
      <script>
        $(document).ready(function() {
          $(".jOrgChart-chart").jOrgChart({
            chartElement:"#orgChart"
          });
          $(".jOrgChart-chart").hide();
        });
</script>
      ';
    }
    add_action('wp_footer', 'jqueryOrgChartInit');
}


