<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div id="container" class="row">
	<div class="large-3 columns">
		<?php echo get_sidebar(); ?>
	</div>
	<div style="background-color:#fff;" class="small-12 large-9 columns">
		<!-- Orbit slider -->
		<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();

				/*
				 * Include the post format-specific template for the content. If you want to
				 * use this in a child theme, then include a file called called content-___.php
				 * (where ___ is the post format) and that will be used instead.
				 */
				?>
				<h3><?php the_title(); ?></h3>
				<hr>
				<?php
				the_content();

				// Previous/next post navigation.
			endwhile;
		?>
		
	</div>
</div>

<?php
get_footer();