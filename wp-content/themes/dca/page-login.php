<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div id="container" class="row">
	<div class="large-3 columns">
		<?php echo get_sidebar('left'); ?>
	</div>
	<div style="background-color:#fff" class="small-12 large-9 columns">
		<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();

				/*
				 * Include the post format-specific template for the content. If you want to
				 * use this in a child theme, then include a file called called content-___.php
				 * (where ___ is the post format) and that will be used instead.
				 */
			?>
			<h3><?php the_title();?></h3>
			<hr>
			<div style="padding:80px 0px 80px 0px;text-align:center;" class="row">
				<div class="medium-3 columns medium-offset-3">
					<a href="#login">
					<img src="<?= get_template_directory_uri(); ?>/images/ico_public.png" alt="Public Login" />
					<br>
					Public Login
					</a>
				</div>
				<div class="medium-3 columns end">
					<a href="#login">
					<img src="<?= get_template_directory_uri(); ?>/images/ico_admin.png" alt="Employee Login" />
					<br>
					Employee Login
					</a>
				</div>
			</div>
			<?php
				the_content();
			endwhile;
		?>
		
	</div>
</div>
<?php
get_footer();