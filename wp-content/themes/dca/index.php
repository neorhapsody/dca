<?php echo get_header(); ?>
<div id="container" class="row">
	<div class="small-12 large-12 columns">
		<!-- Orbit slider -->
		<div class="row"> 
			<div class="small-12 medium-9 large-9 columns">

				<div class="row hide-for-small-only">
					<div class="small-12 medium-12 large-12 columns">
						<?php if ( dynamic_sidebar('Home Page Center Widget') ) : else : endif; ?>
					</div>
				</div>

				<div class="row news-item">
					<div class="small-12 medium-6 large-6 columns">
						<?php if ( dynamic_sidebar('Home Page Widget 1') ) : else : endif; ?>
					</div>
					<div class="small-12 medium-6 large-6 columns">
						<?php if ( dynamic_sidebar('Home Page Widget 2') ) : else : endif; ?>	
					</div>
				</div>

			</div>
			<div class="medium-3 large-3 columns">
				<?php echo get_sidebar(); ?>
			</div>
		</div>
		
	</div>
</div>
<?php echo get_footer(); ?>

