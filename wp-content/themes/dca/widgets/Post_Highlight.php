<?php
class Post_Highlights extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'post_highlights', // Base ID
			__('Post Highlights', 'post-highlight-widget'), // Name
			array( 'description' => __( 'a widget that shows post in highlighted categories', 'post-highlight-widget' ), ) // Args
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		// outputs the content of the widget
		?>
		<li class="widget">
			<h2 class="widgettitle"><?php _e('Highlights','post-highlight-widget'); ?></h2>

		<?php
		$args = array(
			'posts_per_page'   => 5,
			'offset'           => 0,
			'category_name'    => 'highlight',
			'orderby'          => 'post_date',
			'order'            => 'DESC',
			'post_type'        => 'post',
			'post_status'      => 'publish',
			'suppress_filters' => true );

		$posts_array = get_posts( $args );

		foreach ($posts_array as $post) :
			$title = apply_filters('the_title',$post->post_title);
			$content = strip_tags(apply_filters('the_content',$post->post_content));
			if(strlen($content) > 140)
			    $content = substr($content,0,140)."...";
		?>

		<div class="row">
			<div class="small-12 medium-12 large-12 columns">
			<div class="highlight-item">
				<h4><?php echo $title; ?></h4>
				<p><?php echo $content; ?></p>
			</div>
			</div>
		</div>

		<?php
		endforeach;

		$link = $instance['youtube_url'];
		if ( ! empty( $link) ) :
			parse_str(parse_url($link,PHP_URL_QUERY),$out);
		?>
		<div class="row">
			<div class="small-12 medium-12 large-12 columns">
				<div class="flex-video">
				<iframe width="100%" height="200" src="//www.youtube.com/embed/<?php echo $out['v']; ?>" frameborder="0" allowfullscreen></iframe>
			</div>
			</div>
		</div>
		<?php
			if (isset($instance['channel_url'])) :
		?><div class="clearfix" style="padding-right:5px">
			<a class="right" target="_blank" href="<?php echo $instance['channel_url']; ?>">More Videos >></a>
			</div>
		<?php
			endif;
		endif;
		?>

		</li>
		<?php
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin		
		if ( isset( $instance[ 'youtube_url' ] ) ) {
			$link = $instance[ 'youtube_url' ];
		} else {
			$link = 'http://www.youtube.com/watch?v=tRIV5Ba2nFU';
		}

		if ( isset( $instance[ 'channel_url'] ) ) {
			$channel = $instance['channel_url'];
		} else {
			$channel = 'http://www.youtube.com/channel/UC1GIg4ORGRDGtYtu4ulPNDg'; //random airplane channel
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'youtube_url' ); ?>"><?php _e( 'Youtube URL:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'youtube_url' ); ?>" name="<?php echo $this->get_field_name( 'youtube_url' ); ?>" type="text" value="<?php echo esc_attr( $link ); ?>">
		</p>
		<p>
		<label for="<?php echo $this->get_field_id( 'channel_url' ); ?>"><?php _e( 'Youtube Channel URL:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'channel_url' ); ?>" name="<?php echo $this->get_field_name( 'channel_url' ); ?>" type="text" value="<?php echo esc_attr( $channel ); ?>">
		</p>
		<?php 
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['youtube_url'] = ( ! empty( $new_instance['youtube_url'] ) ) ? strip_tags( $new_instance['youtube_url'] ) : '';
		$instance['channel_url'] = ( ! empty( $new_instance['channel_url'] ) ) ? strip_tags( $new_instance['channel_url'] ) : '';

		return $instance;
	}
}