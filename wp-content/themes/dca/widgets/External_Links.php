<?php
class External_Links extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'external_links', // Base ID
			__('Related Links', 'highlighted posts widget title'), // Name
			array( 'description' => __( 'A widget that show exteranal link site links', 'External links widget description' ), ) // Args
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		// outputs the content of the widget
		$title = isset($instance['title']) && !empty($instance['title']) ? $instance['title'] : 'External Links';
		?>
		<li class="widget">
			<h2 class="widgettitle"><?php echo $title; ?></h2>
			<?php

			$links = isset($instance['links']) && !empty($instance['links']) ? unserialize($instance['links']) : array();
			foreach ($links as $link) :
				$site_url = $link['site_url'];
				$image_url = $link['image_url'];
				?>
				<a href="<?php echo $site_url?>"><img src="<?php echo $image_url; ?>"></a>
				<?php
			endforeach;
			?>
		</li>
		
    	<?php
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin		
		$max_number_of_links = 6;
		$links = isset($instance['links']) && !empty($instance['links']) ? unserialize($instance['links']) : array();

?>
		<p>
		<label for="<?php echo $this->get_field_id( 'site_url' ); ?>"><?php _e( 'Site url:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'site_url' ); ?>" name="<?php echo $this->get_field_name( 'site_url' ); ?>[]" type="text" value="<?php echo esc_attr( $site_url ); ?>">
		</p>
<?php
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['limit'] = ( ! empty( $new_instance['limit'] ) ) ? strip_tags( $new_instance['limit'] ) : '3';

		return $instance;
	}
}