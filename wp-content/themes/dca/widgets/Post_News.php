<?php
class Post_News extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'post_news', // Base ID
			__('Post News', 'highlighted posts widget title'), // Name
			array( 'description' => __( 'a widget that shows post in highlighted categories', 'Highlighted posts widget description' ), ) // Args
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		// outputs the content of the widget
		?>
		<li class="widget">
			<h2 class="widgettitle">News</h2>
		<?php
		$limit = 3;
		if ( isset($instance['limit']))
		{
			$limit = $instance['limit'];
		}

		$category_name = 'news';
		$args = array(
			'posts_per_page'   => $limit,
			'offset'           => 0,
			'category_name'    => $category_name,
			'orderby'          => 'post_date',
			'order'            => 'DESC',
			'post_type'        => 'post',
			'post_status'      => 'publish',
			'suppress_filters' => true 
		);

		$posts_array = get_posts( $args );

		foreach ($posts_array as $post) :
			$args = array( 
				'post_type' => 'attachment', 
				'posts_per_page' => -1, 
				'post_status' =>'any', 
				'post_parent' => $post->ID 
			); 

			$attachments = get_posts( $args );
			if ( $attachments ) {
				$attachment = $attachments[0];
			}


			$title = apply_filters('the_title',$post->post_title);
			$content = apply_filters('the_content',$post->post_content);
			$post_link = $post->guid;
			if(strlen($content) > 140)
			    $content = substr($content,0,140)."...";
		?>

		<div class="row news-item">
			<div class="medium-4 large-4 columns">
				<?php
					if (isset($attachment)) {
						echo wp_get_attachment_image($attachment->ID, 'thumbnail');
					}
				?>
			</div>
			<div class="small-12 medium-8 large-8 columns">
				<h6 class="subheader news-title">
					<a href="<?php echo $post_link; ?>"><?php echo $title; ?></a>
				</h6> 
			</div>
		</div>

		<?php
		endforeach;
		$category_id = get_cat_ID( $category_name );
    	$category_link = get_category_link( $category_id );
    	?>
		<div class="row news-more-link">
			<div class="small-12 medium-12 large-12 columns">
				<a style="margin-right:5px" class="right" href="<?php echo esc_url( $category_link ); ?>">More News >></a>
			</div>
		</div>		
    	</li>
	    	
    	<?php
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin		
		if ( isset( $instance[ 'limit' ] ) ) {
			$limit = $instance[ 'limit' ];
		} else {
			$limit = 3;
		}

?>
		<p>
		<label for="<?php echo $this->get_field_id( 'limit' ); ?>"><?php _e( 'Number of news item:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'limit' ); ?>" name="<?php echo $this->get_field_name( 'limit' ); ?>" type="text" value="<?php echo esc_attr( $limit ); ?>">
		</p>
<?php
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['limit'] = ( ! empty( $new_instance['limit'] ) ) ? strip_tags( $new_instance['limit'] ) : '3';

		return $instance;
	}
}