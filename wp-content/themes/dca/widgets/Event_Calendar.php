<?php
class Event_Calendar extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'event_calendar', // Base ID
			__('Event Calendar', 'Show calendar'), // Name
			array( 'description' => __( 'calendar utilizing foundation', 'Show Calendar' ), ) // Args
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		$title = 'Calendar of Events';
		if(isset($instance['title']))
		{
			$title = $instance['title'];
		}
?>

		<li class="widget">
			<h2 class="widgettitle"><?php echo $title; ?></h2>

		<div class="row event-calendar">
			<div class="small-12 medium-12 large-12 columns">
<?php
		get_calendar( true, true );
?>
			</div>
		</div>
		</li>
<?php
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {

	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
	}
}