<?php


class Top_links extends WP_Widget {

	public function __construct() {
		parent::__construct(
			'top_links', // Base ID
			__('Top Links', 'top links'), // Name
			array( 'description' => __( 'a widget that shows top links', 'top links' ), ) // Args
		);
	}


	public function widget($args, $instance){

		$login_url = $instance['login_url'];
		$login_text = __('Login','top links widget');

//		$mobile_portal_url = $instance['mobile_portal_url'];
//		$mobile_portal_text = __('Mobile Portal','top links widget');

//		$feedback_url = $instance['feedback_url'];
//		$feedback_text = __('Feedback','top links widget');

		$faq_url = $instance['faq_url'];
		$faq_text = __('FAQ','top links widget');
?>
		<a href="<?=$login_url?>"><?=$login_text?></a> |
		<!--<a href="<?=$mobile_portal_url?>"><?=$mobile_portal_text?></a> |-->
		<!--<a href="<?=$feedback_url?>"><?=$feedback_text?></a> | -->
		<a href="<?=$faq_url?>"><?=$faq_text?></a>

<?php
	}

	public function form($instance){
		$login_url = $instance['login_url'];
		//$mobile_portal_url = $instance['mobile_portal_url'];
		$feedback_url = $instance['feedback_url'];
		$faq_url = $instance['faq_url'];
?>
		<p>
		<label for="<?php echo $this->get_field_id( 'login_url' ); ?>"><?php _e( 'Login Url:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'login_url' ); ?>" name="<?php echo $this->get_field_name( 'login_url' ); ?>" type="text" value="<?php echo esc_attr( $login_url ); ?>">
		</p>
<!--
		<p>
		<label for="<?php echo $this->get_field_id( 'mobile_portal_url' ); ?>"><?php _e( 'Mobile Portal Url:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'mobile_portal_url' ); ?>" name="<?php echo $this->get_field_name( 'mobile_portal_url' ); ?>" type="text" value="<?php echo esc_attr( $mobile_portal_url ); ?>">
		</p>
-->

<!--
		<p>
		<label for="<?php echo $this->get_field_id( 'feedback_url' ); ?>"><?php _e( 'Feedback Url:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'feedback_url' ); ?>" name="<?php echo $this->get_field_name( 'feedback_url' ); ?>" type="text" value="<?php echo esc_attr( $feedback_url ); ?>">
		</p>
-->
		<p>
		<label for="<?php echo $this->get_field_id( 'faq_url' ); ?>"><?php _e( 'FAQ Url:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'faq_url' ); ?>" name="<?php echo $this->get_field_name( 'faq_url' ); ?>" type="text" value="<?php echo esc_attr( $faq_url ); ?>">
		</p>
<?php
	}


	public function update($newInstance, $oldInstance){
		$instance['login_url'] = $newInstance['login_url'];
		//$instance['mobile_portal_url'] = $newInstance['mobile_portal_url'];
		$instance['feedback_url'] = $newInstance['feedback_url'];
		$instance['faq_url'] = $newInstance['faq_url'];

		return $instance;
	}
}