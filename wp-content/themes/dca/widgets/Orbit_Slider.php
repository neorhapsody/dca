<?php
class Orbit_Slider extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'orbit_slider', // Base ID
			__('Orbit Slider', 'image slider'), // Name
			array( 'description' => __( 'a widget for image slider based on foundation framework', 'image slider widget' ), ) // Args
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		?>
		<ul style="height:500px"class="example-orbit" data-orbit data-options="variable_height:true;bullets:false;">
		<?php if(!empty($instance["image1"])) : ?>
		  <li>
		    <img style="margin:0 auto" src="<?php echo $instance["image1"]; ?>" alt="slide 1" />
		    <div class="orbit-caption">
		      <?php echo $instance["caption1"]; ?>
		    </div>
		  </li>
		<?php endif; ?>

		<?php if(!empty($instance["image2"])) : ?>
		  <li>
		    <img style="margin:0 auto" src="<?php echo $instance["image2"]; ?>" alt="slide 1" />
		    <div class="orbit-caption">
		      <?php echo $instance["caption2"]; ?>
		    </div>
		  </li>
		<?php endif; ?>

		<?php if(!empty($instance["image3"])) : ?>
		  <li>
		    <img style="margin:0 auto" src="<?php echo $instance["image3"]; ?>" alt="slide 1" />
		    <div class="orbit-caption">
		      <?php echo $instance["caption3"]; ?>
		    </div>
		  </li>
		<?php endif; ?>
		</ul>
		<?php
		
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin

		$image1 = isset( $instance[ 'image1' ] ) ? $instance[ 'image1' ] : '';
		$caption1 = isset( $instance[ 'caption1' ] ) ? $instance[ 'caption1' ] : '';
		$image2 = isset( $instance[ 'image2' ] ) ? $instance[ 'image2' ] : '';
		$caption2 = isset( $instance[ 'caption2'  ] ) ? $instance[ 'caption2'  ] : '';
		$image3 = isset( $instance[ 'image3' ] ) ? $instance[ 'image3' ] : '';
		$caption3 = isset( $instance[ 'caption3' ] ) ? $instance[ 'caption3' ] : '';
?>

		<p>
		<label for="<?php echo $this->get_field_id( 'image1' ); ?>"><?php _e( 'Image URL:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'image1' ); ?>" name="<?php echo $this->get_field_name( 'image1' ); ?>" type="text" value="<?php echo esc_attr( $image1 ); ?>">
		</p>

		<p>
		<label for="<?php echo $this->get_field_id( 'caption1' ); ?>"><?php _e( 'Caption:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'caption1' ); ?>" name="<?php echo $this->get_field_name( 'caption1' ); ?>" type="text" value="<?php echo esc_attr( $caption1 ); ?>">
		</p>

		<p>
		<label for="<?php echo $this->get_field_id( 'image2' ); ?>"><?php _e( 'Image URL:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'image2' ); ?>" name="<?php echo $this->get_field_name( 'image2' ); ?>" type="text" value="<?php echo esc_attr( $image2 ); ?>">
		</p>

		<p>
		<label for="<?php echo $this->get_field_id( 'caption2' ); ?>"><?php _e( 'Caption:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'caption2' ); ?>" name="<?php echo $this->get_field_name( 'caption2' ); ?>" type="text" value="<?php echo esc_attr( $caption2 ); ?>">
		</p>

		<p>
		<label for="<?php echo $this->get_field_id( 'image3' ); ?>"><?php _e( 'Image URL:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'image3' ); ?>" name="<?php echo $this->get_field_name( 'image3' ); ?>" type="text" value="<?php echo esc_attr( $image3 ); ?>">
		</p>
		<p>
		<label for="<?php echo $this->get_field_id( 'caption3' ); ?>"><?php _e( 'Caption:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'caption3' ); ?>" name="<?php echo $this->get_field_name( 'caption3' ); ?>" type="text" value="<?php echo esc_attr( $caption3 ); ?>">
		</p>
<?php
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['image1'] = ( ! empty( $new_instance['image1'] ) ) ? strip_tags( $new_instance['image1'] ) : '';
		$instance['image2'] = ( ! empty( $new_instance['image2'] ) ) ? strip_tags( $new_instance['image2'] ) : '';
		$instance['image3'] = ( ! empty( $new_instance['image3'] ) ) ? strip_tags( $new_instance['image3'] ) : '';

		$instance['caption1'] = ( ! empty( $new_instance['caption1'] ) ) ? strip_tags( $new_instance['caption1'] ) : '';
		$instance['caption2'] = ( ! empty( $new_instance['caption2'] ) ) ? strip_tags( $new_instance['caption2'] ) : '';
		$instance['caption3'] = ( ! empty( $new_instance['caption3'] ) ) ? strip_tags( $new_instance['caption3'] ) : '';

		return $instance;
	}
}