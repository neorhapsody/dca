<?php
class SectorDivision_Slider extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'sector_division_slider', // Base ID
			__('Sector and Division Slider', 'sector and division slider'), // Name
			array( 'description' => __( 'a widget that shows sector and division in slides', 'sector and division slider' ), ) // Args
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		$menu = wp_get_nav_menu_object( 'sector-division-menu' );
		$menu_items = wp_get_nav_menu_items( $menu->term_id );
?>

		<li class="widget sector-division" style="border-radius:0px 0px 10px 10px">
		<h2 class="widgettitle"><?php _e('Sector & Division','post-highlight-widget'); ?></h2>
		<ul class="sector-division-slider" data-orbit data-options="animation:fade;timer:false;bullets:false;slide_number:false;pause_on_hover:false;">
			<?php $count = 1; ?>
			<?php foreach (array_chunk($menu_items, 5) as $menus) : ?>
				<li data-orbit-slide="headline-<?=$count?>">
				<?php foreach ($menus as $menu) : ?>
				    <a href="<?=$menu->url?>">
				    	<span class="sections-and-divisions-ico ico-<?php implode('-',explode(' ',strtolower($menu->title))); ?>"></span>
				      
				      <?=$menu->title?>
				    </a><br>
				<?php endforeach; ?>
				</li>
			<?php $count++;endforeach; ?>
		</ul>
		</li>
<?php
	}

	/**
	 * Ouputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {

	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		return;
	}
}