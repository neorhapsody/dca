<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header>
		<h3 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
		</h3>
	</header>
<?php if ( is_search() ) :  ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div>
	<?php else : ?>
	<div class="entry-content">
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'dca theme' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'dca theme' ), 'after' => '</div>' ) ); ?>
	</div>
<?php endif; ?>
</article>