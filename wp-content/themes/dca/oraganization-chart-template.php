<?php
/*
Template Name: Organization Chart
*/
add_action( 'wp_enqueue_scripts', 'organization_chart_scripts' );
get_header(); ?>
<div id="container" class="row">
	<div class="large-3 columns">
		<?php echo get_sidebar( 'left' ); ?>
	</div>
	<div class="small-12 large-9 columns">
		<div id="orgChart">
		<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();

			?>
			<h3><?php the_title();?></h3>
			<hr>
			<?php
				the_content();
			endwhile;
		?>
		</div>
		
	</div>
</div>
<?php get_footer(); ?>