<div id="footer-row" class="row">
	<div class="small-12 medium-4 large-4 columns">

		<ul id="footer-address"class="vcard">
		  <li class="fn">Jabatan Penerbangan Awam Malaysia</li>
		  <li class="street-address">No. 27, Persiaran Perdana</li>
		  <li class="locality">Aras 1-4, Blok Podium 62618 Putrajaya</li>
		  <li class="phone">Tel: +60388714000</li>
		  <li class="email">Email: <a href="#">webmaster@dca.gov.my</a></li>
		</ul>
		
	</div>
	<div class="small-12 medium-8 large-8 columns">
		<nav class="sitelinks right">
			<ul>
				<li><a href="#">Privacy Policy</a> &nbsp;|&nbsp; </li>
				<li><a href="#">Security Policy</a> &nbsp;|&nbsp; </li>
				<li><a href="#">Copyright</a> &nbsp;|&nbsp; </li>
				<li><a href="#">Disclaimer</a> &nbsp;|&nbsp; </li>
				<li><a href="#">Sitemap</a> &nbsp;|&nbsp; </li>
				<li><a href="#">Support</a></li>
			</ul>
		</nav>
		<br>	
		<div id="footer-copyright" class="right" style="text-align:right">
			&copy; Copyright Department of Civil Aviation Malaysia <br>
			Last Updated: <?php the_modified_date(); ?> <br> <br>
			<!-- BEGIN: Powered by Supercounters.com -->
			<div style="display:inline-block">You are visitor: </div>
			<center style="display:inline-block">
				<script type="text/javascript" src="http://widget.supercounters.com/hit.js"></script>
				<script type="text/javascript">sc_hit(823273,4,5);</script><br>
				<noscript><a href="http://www.supercounters.com">Tumblr Hit Counter</a></noscript>
			</center>
			<!-- END: Powered by Supercounters.com -->

		</div>
	</div>
</div>


  </div>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/components/foundation/js/foundation.min.js"></script>

<script type="text/javascript">
	$(document).foundation({
		topbar : {
		    custom_back_text: false,
		    is_hover: false,
		    mobile_show_parent_link: true
		  }
	});

	function allTextElements(){
		return $('p, a, li, h1, h2, h3, h4, h5, h6');
	}
	var newFontSize = 100;
	$(document).ready(function(){
	  //W3C accessibility
	  $(".tools.font-size-reset").click(function(){
	    newFontSize = 100;
	    allTextElements().css('font-size','');
	  });

	  $(".tools.font-size-increase").click(function(){
	    newFontSize = newFontSize*1.1;
	    allTextElements().css('font-size',newFontSize + '%');
	    return false;
	  });

	  $(".tools.font-size-decrease").click(function(){
	  	if(newFontSize == 100){ return; } //dont go lower than 100%
	    newFontSize = newFontSize/1.1;
	    allTextElements().css('font-size',newFontSize + '%');
	    return false;
	  });

	  $.each(['red','blue','green'], function(index, color){
	  	$(".tools.font-color-"+color).click(function(){
		  allTextElements().css('color',color);
		});
	  });

	  $(".tools.font-color-reset").click(function(){
	   allTextElements().css('color','');
	  });

	});
</script>
<?php echo wp_footer(); ?>
</body>
</html>